#!usr/bin/python 3
#HangmanJJ game v computer generated word
#John Johnson
#200530

from random import random

new_word_list=[]
count=0

string_ans_word=""
string_hang_word=""

def getword():
    global string_ans_word
    global string_hang_word
    filename = 'hangman_words.txt'
    with open(filename,'r') as reader:
        whole_text = reader.read()
        whole_text = whole_text.upper()
        word_list = whole_text.split()        
        for word in word_list:
            if 5<len(word)<10:
                new_word_list.append(word)
        leng=len(new_word_list)      
        num=int(random()*leng-1) 
        string_hang_word=new_word_list[num]         
        for item in string_hang_word:
            string_ans_word=string_ans_word+"-"
        print(string_hang_word)
        print(string_ans_word)                                            #Comment back in for testing
        return string_ans_word

def getletter():    
    flag =True
    global count
    while flag:        
        letter=input("Enter a letter  ")
        letter=letter.upper()
        if len(letter)!=1:                           
            print("single letter only")           
        elif 64<ord(letter)<91:                  
            flag=False
        else:
            print('Please enter a letter between "A" and "Z"')            
    return letter    

def getandtest():
    totlet=""                                     #letters used so far
    global count                                  #number of guesses
    global string_ans_word
    count=0
    while string_hang_word!=string_ans_word and count<15:    #criteria for finishing          
        letter=getletter()                        #fetch another letter
        letter=letter.upper()
        if len(letter)==1:
            totlet=totlet+letter                  #update record of letter guesses           
        build=""                                  #build new ans word from
        ind=0                                     #existing ans word plus new letter
        leng=len(string_hang_word)
        for ind in range(leng):        
            if string_hang_word[ind]!= letter:
                build = build+string_ans_word[ind]               
            else:  
               build= build + letter        
        string_ans_word=build                     #return built word to ans word
        print(string_ans_word, "\t\t\t\t", totlet," \t\t\t",count, "lives used")         #update the answer list on screen
        if string_ans_word !=build:            
            count+=1                             #number of trys and loop back for more
        print()        
    if count <15:
        print("YOU WIN!! The word is indeed", string_ans_word)
    elif count>14:
        print("You lose, the word is ",string_hang_word) 
    print("You took",count+1,"tries to complete the puzzle.\n\nGreater than 15 you are dead.")
    print()  
    return "Thank-you for playing"

#--------------------------------------------------------------------------------------------
string_ans_word=getword()
print (getandtest())

