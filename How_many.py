
# File Refreshments.py

# Mark
# 10/11/20

# Takes requests for beverages, makes and delivers them.

people_min = 0  # Minimum number of people to make drinks for.
people_max = 6  # Maximum number of people to serve - Limited by kettle
capacity.
beverages=['Tea','Coffee','Chocolate']
extra_sugar = 0 # Do they want sugar?
extra_milk = False #Do they want milk?


def how_many(question,pmin,pmax):
    # Function to get an interger in the range pmin to pmax from the user.

    count='a'
    while not count.isdigit():
        count=input(question)
        if not count.isdigit():
            print('Thats not a number')
    count=int(count)
    while count < pmin or count > pmax:
        # Value is not in range!
        print('thats out of range must be between ', pmin, ' and ',pmax)
        count=int(input(question))

    return count


guests=how_many('Who wants a drink, put your hand up:',people_min,people_max)
print('Guests:',guests)

sugars=how_many('How many sugars? :',0,3)
print('Sugar',sugars)

age=how_many('How old are you? :',1,99)
print('Age',age)

milk=how_many('How many ml of milk? :',2,20)
print('Milk',milk)

