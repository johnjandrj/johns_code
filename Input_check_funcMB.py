
def get_input(l_limit,u_limit,default,prompt_str):
    # Prompt the user to enter a number between the required limits
    out_of_range=True
    while out_of_range:
        try:
            i=int(input (prompt_str))
            if l_limit<=i<=u_limit:
                out_of_range=False 
            else:
                print('Entry out of range, try again')
        except ValueError:            
            return default
    return i

#________________ MAIN _______________  

# Ask for a y horizontal & x vertical sizes for the grid.
x=get_input(1,12,4,'Horizontal grid size between 1 and 12: ')
y=get_input(1,12,5,'Vertical grid size between 1 and 12: ')
# Ask for the vertical spacing size.
z=get_input(3,6,3,'Vertical spacing size between 3 and 6: ')-2

