#! python 3.7.4
#Basic_Menu_Exercise.py
#J Johnson 18-04-2020
#Demos use of Dictionary, code and character conversion and while loop control

print('Basic Menu:\n')      
dict = {'P':'Print','S':'Save','D':'Delete','R':'Run','E':'Edit','X':'eXit'}#Use dictionary to display menu

answer='YES' #flag to run loop code first time
while answer=="YES":
    print(dict) #Displays menu by printing dictionary
    
    n=input('\nSelect an option:') # select option from menu.
    if n=='':
        n='z'  #Null entry requires dummy entry to process next line without error report       
    n=n[0]#protects against more than one character input by reducing it to first character only
   
    if n =='S' or n=='P' or n=='D'or n=='R' or n=='E' or n=='X':   #chr codes for uppercase letters D to X
        choice=dict[n]# converts integer back to character to match dictionary
        print()
        print(choice,"option selected ")
        answer='' #Flag To exit while loop
        
    else:
        print('\nYou have not chosen a valid option')
        answer=input('\nTo try again enter YES  ') #Will set flag to restart while loop
        if answer!= "YES": #Flag to exit while loop
            print("\nYou have chosen to exit without making a choice from the menu")
            
     