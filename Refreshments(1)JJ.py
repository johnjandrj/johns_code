# File Refreshments1.py
# 
# JJ 20/12/03
# Takes requests for beverages, makes and delivers them.


people_min = 0     # Minimum number of people to make drinks for.
people_max = 6     # Maximum number of people to serve - Limited by kettle capacity.
beverages=['tea','coffee','chocolate','beer']
bev_keys=['1','2','3','4','5','6','7','8','9']
milk = False       #Do they want milk?
beverage_min=1
beverage_max=len(beverages)
sugar_min=0
sugar_max=6
beverages_to_make = {}
zipped=dict(zip(bev_keys, beverages))

def get_int(question,imin,imax):     # Function to get an interger in the range pmin to pmax from
                                     # the user.
    while True:
        num_str=input(question)      # Ask the quesion        
        if not num_str.isdigit():    # Is the entry a number?
            print('Sorry that does not appear to be a number')
            print()
        else:
            num=int(num_str)         # Turn the entry into an intger and check it's in range
            if  imin > num or num > imax:   # Value is not in range!            
                print('That is out of range, entry must be between ', imin, ' and ',imax)
            else:
                print()
                return num           # Return the number - Also terminates while loop and the functionbeverage_max=len(beverages)


guests=get_int('How many people to make drinks for:',people_min,people_max)

if guests>0:                         # 1 or more person wants a drink
    for item in range(guests):       # for each guest ask what they want
        milk=False
        val=[]
        guest_name=input('name? ')   # Enters guest name
        print()
        print('drinks available are: ')
        for bev in range (1,(beverage_max)+1):        
            print(str(bev)+' '+beverages[bev-1])#mark's beverage list
        print()
        for key,value in zipped.items():
            print(key,value)          #prints john's beverage list            
        print()
        guest_beverage_num=get_int('Please enter number key for required beverage ', beverage_min,beverage_max)
        guest_beverage=beverages[(guest_beverage_num)-1]
        print('You have selected ',guest_beverage)
        sugar=get_int('Do you want sugar? 0 to 6 tsps.',0,sugar_max)
        milk_ans=input('Do you want milk? y or n ')
        if bool(milk_ans=='y'or milk_ans=='Y'):milk=True        
        if 7>=sugar>=0 : sugar= str(sugar)+' tsp  '
        else: sugar='NO'
        val=[guest_name, guest_beverage,sugar,milk]
        
        beverages_to_make.update({guest_name:val})
        print()        
        if item < (guests - 1):
            print('nextguest?  ')
            print()
print('That completes the order for beverages')
print()
print('NAME \tBEV     SUGAR   MILK')
print('------------------------------')

for guest_name in beverages_to_make:        
    details = beverages_to_make.get(guest_name)
    for detail in details:
        print(detail,end="\t")
    print()
    
'''Should our menu be a string, a list or a dictionary?
Do we want to number them as we type them in or should the code number them?
If we number them and then remove / add something from / to the middle
of the list how is it going to get renumbered?'''    