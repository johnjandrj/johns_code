a=['A','B','C']            
b=['1','2','3','4']
c=(9,8,7,6,5)
d={26:'Z',25:'Y',24:'X'}

print(dict(zip(a,b,c,d)))


#print(list(zip(a,b)))       #[('A', '1'), ('B', '2'), ('C', '3')]

#print(list(zip(c,b)))        #[(9, '1'), (8, '2'), (7, '3'), (6, '4')]

#print(dict(zip(a,d)))         #{'A': 26, 'B': 25, 'C': 24}

#print(tuple(zip(d,c)))        #((26, 9), (25, 8), (24, 7))

#x=zip(a,b,c)
#print(x)                       #<zip object at 0x036011E8>

#print(dict(zip(a,c)))           #{'A': 9, 'B': 8, 'C': 7}
'''for n in x:
    for m in n:
        print(m, end='\t')
    print()
print()'''

#y=list(zip(a,b,c))    #[('A', '1', 9), ('B', '2', 8), ('C', '3', 7)]
#print(y)

'''I found the 3rd one interesting in that it will make a dictionary without all that fiddling with colons.

There is masses of stuff on measuring the speed of programs using timing loops but as our code has manual input in it we are going to be the very slow bit! Have a look at; https://docs.python.org/3/library/timeit.html

Regards Mark'''